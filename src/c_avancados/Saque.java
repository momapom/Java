package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {


		List<String> lista = lerContas();

		if(lista == null) {
			System.out.println("Não foi possível ler o arquivo");
			return;
		}

		Scanner scanner = new Scanner(System.in);
		boolean existeId = false;
		boolean header = true;

		System.out.println("ID: ");
		int id = scanner.nextInt();

		System.out.println("Valor do Saque: ");
		int valorSaque = scanner.nextInt();

		StringBuilder saida = new StringBuilder();

		for(String linha: lista) {
			String[] partes = linha.split(",");

			if (header) {
				header = false;
				linha += "\n";
				saida.append(linha);
			} else {

				int idConta = Integer.parseInt(partes[0]);
				int saldoConta = Integer.parseInt(partes[4]);

				if (idConta == id) {
					existeId = true;
					System.out.println("Saldo atual: " + saldoConta);
					if(saldoConta >= valorSaque) {
						int novoSaldo = saldoConta - valorSaque;
						String novoArq = partes[0] + "," + partes[1] + "," + partes[2] + "," + partes[3] + "," + novoSaldo + "\n";
						saida.append(novoArq);
						System.out.println("Saldo apos o saque: " + novoSaldo);

					}
					else
						System.out.println("Saldo insufucuente");
				}
				else {
					linha += "\n";
					saida.append(linha);
				}
			}
		}
		if (!existeId) {
			System.out.println("ID não encontrado");
		}
		regravarArquivo(saida);

	}

	public static List<String> lerContas(){
		Path path = Paths.get("contas.csv");

		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}

		//		lista.remove(0);

		return lista;
	}

	public static void regravarArquivo(StringBuilder arquivo) {
			Path path = Paths.get("contas.csv");
	
			try {
				Files.write(path, arquivo.toString().getBytes());
			} catch (IOException e) {
				System.out.println("Não foi possível gravar o arquivo");
			}		
		}
}
